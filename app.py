import dash
import dash_core_components as dcc
import dash_html_components as html
from dash import Dash
from dash.dependencies import Input, Output
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, func
import pandas as pd
import numpy as np
from sqlalchemy import inspect
import plotly.plotly as py
import plotly.graph_objs as go
from config import uname, apikey

py.sign_in(uname,apikey)

app = Dash(__name__)
#application = app.server #for elastic beanstalk

#SQL and Python magic
#create BBB engine
engine = create_engine('sqlite:///DataSets/belly_button_biodiversity.sqlite/')

#map base to schema
# All your base are belong to us.
Base = automap_base()

#Reflect
Base.prepare(engine, reflect=True)

#create session#create
session = Session(bind=engine)

# Print all of the classes mapped to the Base
#Base.classes.keys()

# create queries
Otu = Base.classes.otu
Samples = Base.classes.samples
Meta = Base.classes.samples_metadata

#start session
session = Session(engine)

#inspect
#inspector = inspect(engine)

#otu_columns = inspector.get_columns('Otu')
#print(otu_columns)

#query names
#names = session.query(Samples).statement

# df is the best
#names_df = pd.read_sql_query(names,session.bind)

#don't need otu_id
#names_df = names_df.drop('otu_id', axis=1)

#python function = list(df), to_list = pandas function
#names = list(names_df.columns)

def names():
    names = session.query(Samples).statement
    names_df = pd.read_sql_query(names,session.bind)
    names_df = names_df.drop('otu_id', axis=1)

    return list(names_df.columns)

#get all otu descriptions
#otu_descriptions = session.query(Otu.lowest_taxonomic_unit_found).all()

#uravel tuples to use for counting. I hate tuples.
#otu_list = list(np.ravel(otu_descriptions))

#initialize list of names
names = names()

def otu():
    otu_descriptions = session.query(Otu.lowest_taxonomic_unit_found).all()
    return list(np.ravel(otu_descriptions))

#get all sample IDs without the BB prefix (drop BB_)
#sample = names()[0]

# create dictionary of lists of each sample's metadata
def meta(sample):
    #query db
    selected = [Meta.AGE, Meta.BBTYPE, Meta.ETHNICITY, Meta.GENDER, Meta.LOCATION, Meta.SAMPLEID]
    #filter results to use the 3 digit SID
    results = session.query(*selected).filter(Meta.SAMPLEID == sample[3:]).all()

    meta = {}
    for result in results:
        meta['AGE'] = result[0]
        meta['BBTYPE'] = result[1]
        meta['ETHNICITY'] = result[2]
        meta['GENDER'] = result[3]
        meta['LOCATION'] = result[4]
        meta['SAMPLEID'] = result[5]

    return meta

# create alchemy query and > to df

def samples(sample):
    state_ment = session.query(Samples).statement
    samples_df = pd.read_sql_query(state_ment, session.bind)
    samples_df = samples_df.sort_values(by = sample, ascending=False)
    data = {
        'otu_id': samples_df['otu_id'].values.tolist(),
        'sample_values': samples_df[sample].values.tolist()
    }
    return data

#data_sample = samples(sample)


#app and html

app.layout = html.Div(className='container', style={'padding': '10px'}, children= [
				html.Div(className='jumbotron', children=[
					html.H1(style={'textAlign' : 'center'}, children='Belly Button Biodiversity'),
            		html.P(style={'textAlign' : 'center'}, children='Use the interactive charts below to explore the fascinating dataset'),
            		html.H6(style={'textAlign' : 'center'}, children="Your wildtypes don't scare me."),
					]),

			dcc.Dropdown(className="col-md-6 col-offset-md-3", id="name_dropdown",
                options=[
                     {'label' : name, 'value': name} for name in names
                         ],
                 value=names[0]
                 ),

			html.Div(className="col-md-4", id="meta_data"),

            dcc.Graph(id="donut"),

            dcc.Graph(id="bubble")
])
#callbacks

@app.callback(Output('meta_data','children'), [Input('name_dropdown', 'value')])
def update_metadata(name_dropdown):
	meta_sample = meta(name_dropdown)

	return[
			html.H6(children='AGE :' + str(meta_sample['AGE'])),
			html.H6(children='BBTYPE :' + str(meta_sample['BBTYPE'])),
			html.H6(children='ETHNICITY: ' + str(meta_sample['ETHNICITY'])),
			html.H6(children='GENDER: ' + str(meta_sample['GENDER'])),
			html.H6(children='LOCATION: ' + str(meta_sample['LOCATION'])),
			html.H6(children='SAMPLEID: ' + str(meta_sample['SAMPLEID']))
		]

@app.callback(Output('donut','figure'), [Input('name_dropdown', 'value')])
def update_donut(name_dropdown):
	data_sample = samples(name_dropdown) #otu_id, sample values: []

	fig = {
	    'data':[{
	        'values': data_sample['sample_values'][:10],
	        'labels': data_sample['otu_id'][:10],
	        'type':'pie',
	        'hole': 0.4,
	        'hoverinfo': data_sample['otu_id'][:10],
	        'hovertext': data_sample['otu_id'][:10],
	        'marker': {
	            'colors': ['#D3D4A5', '#A9D9A8' '#79BC9B', '#716F6F', '#472835', '#1F1E3C', '#4F4E71', '#6A9F97', '#E8DAB1', '#EBAB59']
	        }
	    }],
	    'layout': {
	        'title' : 'OTU Percentage',
	    }
	}

	return fig

@app.callback(Output('bubble','figure'), [Input('name_dropdown', 'value')])
def update_bubble(name_dropdown):

	data_sample = samples(name_dropdown)

	fig = {
		'data':[{
			'y': data_sample['sample_values'],
			'x': data_sample['otu_id'],
			'mode':'markers',
			'text': data_sample['otu_id'],
			'marker': {
			    'color': data_sample['otu_id'],
			    'size' : data_sample['sample_values'],
			    'colorscale' : 'Viridis'
			}
		}],
		'layout': {
			'title' : 'OTUs by Sample',
			'hovermode': 'closest',
			'xaxis': {
		    	'title' : 'OTU ID'
		}

	}
}

	return fig


#external imports
app.css.append_css({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"})
app.scripts.append_script({"external_url": "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"})


#please run
if __name__ == '__main__':
#	application.run(debug=False) #elastic beanstalk
    app.run(debug=False)
